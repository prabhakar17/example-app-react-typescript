const initialState = {
    isLoading: false,
    offers: [],
    error: []
};

type actionType = {
    type: string,
    payload: []
}

const reducer = (state = initialState, action: actionType) => {
    switch (action.type) {
        case "FETCH_OFFERS_PENDING": {
            return {...state, isLoading: true}
        }
        case "FETCH_OFFERS_FULFILLED": {
            return {...state, isLoading: false, offers: action.payload}
        }
        case "FETCH_OFFERS_REJECTED": {
            return {...state, isLoading: false, error: action.payload}
        }
        default:
            return {...state, offers: []}
    }
};

export default reducer;