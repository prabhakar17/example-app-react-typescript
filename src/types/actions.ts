import { AxiosPromise } from "axios";

export const FETCH_OFFERS = "FETCH_OFFERS";

export interface fetchOffersAction {
    type: typeof FETCH_OFFERS,
    payload: AxiosPromise
}

export type OffersActionTypes=
  | fetchOffersAction

export type AppActions = OffersActionTypes;