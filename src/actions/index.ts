import axios from 'axios';
import { FETCH_OFFERS, AppActions } from "../types/actions";

const fetchOffers = (): AppActions => ({
  type: FETCH_OFFERS,
  payload: axios.get(
    "https://api.holidu.com/rest/v6/search/offers?searchTerm=Mallorca,%20Spanien"
  )
});

export default fetchOffers;
