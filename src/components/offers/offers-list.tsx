import React from "react";
import styles from "./styles";
import { withStyles } from "@material-ui/styles";
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  Grid
} from "@material-ui/core";

type Props = {
  classes: {
    card: string;
    cardMedia: string;
    container: string;
    offerItem: string;
    offerName: string;
    cardContent: string;
  };
  offersList: any;
};

const currencyMapping: { [index: string]: any } = {
  EUR: "€"
};

const OffersList: React.FC<Props> = ({ classes, offersList }) => {
  return (
    <Grid
      container
      justify="space-evenly"
      className={classes.container}
      alignItems="center"
    >
      {offersList?.offers?.map((offer: any) => (
        <Grid key={offer.id} item className={classes.offerItem}>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.cardMedia}
                image={offer.photos[0]["m"]}
                title={offer.details.name}
              />

              <CardContent className={classes.cardContent}>
                <Typography
                  variant="body1"
                  component="h2"
                  className={classes.offerName}
                >
                  {offer.details.name}
                </Typography>
                <Typography variant="body1" component="h2">
                  {currencyMapping[offer.price.currency]}
                  {offer.price.total}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default withStyles(styles)(OffersList);
