import { createStyles } from "@material-ui/core/styles";

const styles = () =>
  createStyles({
    container: {
      maxWidth: 1110,
      margin: 'auto',
      paddingTop: 65
    },
    offerItem: {
        marginBottom: 16
    },
    card: {
      width: 345,
      height: 250,
      maxHeight: 250
    },
    cardMedia: {
      height: 140
    },
    cardContent: {
        textAlign: 'left',
        display: 'flex'
    },
    offerName: {
        width: '80%'
    }

  });

export default styles;
