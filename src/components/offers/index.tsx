import React from "react";
import OffersList from "./offers-list";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import fetchOffersAction from '../../actions';

const Offers: React.FC<any> = ({ fetchOffers, offers, isLoading, error }) => {
  React.useEffect(() => {
    fetchOffers();
  }, [fetchOffers]);

  if(isLoading) return <div>Loading...</div>
  if (error?.message) return <div>Please try again.</div>

  return offers ? <OffersList offersList={offers?.data} /> : <div>No Offer Found.</div>;
};

const mapStateToProps = (state: any) => {
  return {
    offers: state.offers,
    isLoading: state.isLoading,
    error: state.error
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return { fetchOffers: () => dispatch(fetchOffersAction()) }
}

export default connect(mapStateToProps, mapDispatchToProps)(Offers);
